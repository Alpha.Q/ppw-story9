from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User

from .views import index

class IndexTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 302)

    def test_using_index_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'ppw/index.html')
        
    def test_logout_url_exists(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_hello_url_exists(self):
        response = Client().get('/welcome/')
        self.assertEqual(response.status_code, 302)

    def test_change_url_exists(self):
        response = Client().get('/change/')
        self.assertEqual(response.status_code, 302)

    def test_wrong_username_message(self):
        response = Client().get('/?fail')
        self.assertIn('Wrong username or password, Create user first in admin site if you want to register', response.content.decode())

    def test_need_login_message(self):
        response = Client().get('/?mustlogin')
        self.assertIn('You must to log in first to access', response.content.decode())

    def test_logged_out_message(self):
        response = Client().get('/?logout')
        self.assertIn('Logout successful', response.content.decode())

    def test_successful_change_password_message(self):
        response = Client().get('/?change')
        self.assertIn('Change password successful, please login again with your new password', response.content.decode())

    def test_login_logout(self):
        client = Client()

        username = 'salman'
        password = 'abcabc123'
        user = User.objects.create_user(username=username, password=password)
        
        # Login
        response = client.post('/login/', {
            'username': username,
            'password': password
        })

        # Test if login successful
        response = client.get('/welcome/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Welcome', response.content.decode())
        
        # Logout
        response = client.get('/logout/')
        
        # Test if logout successful
        response = client.get('/welcome/')
        self.assertEqual(response.status_code, 302)
    
    def test_login_change_password(self):
        client = Client()

        username = 'salman'
        password = 'abcabc123'
        user = User.objects.create_user(username=username, password=password)
        
        # Login
        response = client.post('/login/', {
            'username': username,
            'password': password
        })

        # Test if login successful
        response = client.get('/welcome/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Welcome', response.content.decode())
        
        # Logout
        response = client.post('/change/', {
            'oldpassword' : 'abcabc123',
            'newpassword' : 'salmanganteng'
        })
        
        # Test if change password and logout successful
        response = client.get('/welcome/')
        self.assertEqual(response.status_code, 302)
