from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User

def index(request):
    response = {}
    if 'mustlogin' in request.GET:
        response['mustlogin'] = True
    if 'logout' in request.GET:
        response['logout'] = True
    if 'fail' in request.GET:
        response['fail'] = True
    if 'change' in request.GET:
        response['change'] = True

    return render(request, 'ppw/index.html', response)

def login(request):
    user = None
    if 'username' in request.POST and 'password' in request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
    
    if user is not None:
        auth_login(request, user)
        return redirect('/welcome')
    else:
        return redirect('/?fail')
        
def welcome(request):
    if request.user.is_authenticated:
        response = {
            'first_name': request.user.first_name,
            'last_name': request.user.last_name
        }

        if request.user.first_name == '' and request.user.last_name == '':
            response['first_name'] = 'Anonymous'
            response['last_name'] = ''

        return render(request, 'ppw/welcome.html', response)
    else:
        return redirect('/?mustlogin')

def logout(request):
    if request.user.is_authenticated:
        auth_logout(request)
        return redirect('/?logout')
    else:
        return redirect('/?mustlogin')

def change(request):
    if request.user.is_authenticated:
        response = {
            'first_name': request.user.first_name,
            'last_name': request.user.last_name
        }

        if request.user.first_name == '' and request.user.last_name == '':
            response['first_name'] = 'Anonymous'
            response['last_name'] = ''

        if 'oldpassword' in request.POST and 'newpassword' in request.POST:
            oldpassword = request.POST['oldpassword']
            newpassword = request.POST['newpassword']

            user = authenticate(request, username=request.user.username, password=oldpassword)

            if user is not None:
                u = User.objects.get(username = request.user.username) 
                u.set_password(newpassword)
                u.save()

                auth_logout(request)
                return redirect('/?change')
            else:
                response['wrongpassword'] = True

        return render(request, 'ppw/welcome.html', response)
    else:
        return redirect('/?mustlogin')

    
